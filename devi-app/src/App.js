import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import AppHeader from './component/AppHeader'
import AppBody from './component/AppBody'


class App extends Component {
  render() {
    return (
      <div>
        <div>
          <AppHeader/>
        </div>
        <div>
          <AppBody/>
        </div>
      </div>
    );
  }
}

export default App;
