import React, { Component } from 'react';
import headerNav from './inside/headerNav';


export class AppHeader extends Component {
  render() {
    return (
      <div>
        <headerNav/>
      </div>
    )
  }
}

export default AppHeader
